// const User = require('../models/User');
// const Product = require('../models/Product');
// const Order = require('../models/Order');
// const bcrypt = require('bcrypt');
// const auth = require('../auth')

// const { request } = require("express")


// // // Start of Retrieve all users (Admin only)
// // module.exports.getAllUsers = (userData) => {

// // 	return User.findById(userData.id).then(result => {

// // 		if(result.isAdmin === true) {
// // 			return User.find({isAdmin: false}, "_id firstName lastName email mobileNo address isAdmin").then(result => {

// // 				if(result.length > 0) {

// // 					return result
// // 				} else{

// // 					return 'User in empty'
// // 				}
// // 			})
// // 		} else {

// // 			return 'Authorized admin only'
// // 		}
// // 	})
// // }
// // // End of Retrieve all users (Admin only)


// // // Start of Setting user as admin (Admin only)
// // module.exports.setAdmin = (data) => {

// // 	return User.findById(data.userId).then(result => {

// // 		if(data.isAdmin === true) {

// // 			if(result.isAdmin === true) {

// // 				return 'Already a admin'
// // 			}

// // 			result.isAdmin = true;

// // 			return result.save().then((setAdmin ,error) => {

// // 				if(error) {

// // 					return 'Failed to become admin'
// // 				} else {

// // 					return 'User become admin succesfully'
// // 				}
// // 			})
// // 		} else {

// // 			return 'Authorized admin only'
// // 		}
// // 	}).catch(error => {

// // 		return 'Sorry, something went wrong please try again.'
// // 	})
// // }
// // // End of Setting user as admin (Admin only)


// // // Start of Setting admin as user (Admin only)
// // module.exports.removeAdmin = (data) => {

// // 	return User.findById(data.userId).then(result => {

// // 		if(data.isAdmin === true) {

// // 			if(result.isAdmin === false) {

// // 				return 'Already a user'
// // 			}

// // 			result.isAdmin = false;

// // 			return result.save().then((removedAdmin ,error) => {

// // 				if(error) {

// // 					return 'Failed to remove as admin'
// // 				} else {

// // 					return 'Successfully remove as admin'
// // 				}
// // 			})
// // 		} else {

// // 			return 'Authorized admin only'
// // 		}
// // 	}).catch(error => {

// // 		return 'Sorry, something went wrong please try again.'
// // 	})
// // }
// // // End of Setting admin as user (Admin only)


// // // Start of Creating Product (Admin only)
// // module.exports.addProduct = (data, requestBody) => {

// // 	return User.findById(data.id).then(result => {

// // 		if(result.isAdmin === true) { 

// // 			let newProduct = new Product ({

// // 				name: requestBody.name,
// // 				description: requestBody.description,
// // 				price: requestBody.price,
// // 				stocks: requestBody.stocks,
// //                 category: requestBody.category
// // 			})

// // 			return newProduct.save().then((product, error) => {

// //                 if(error) {

// //                     return false
// //                 } else {

// //                     return true
// //                 }
// //             })	
// // 		} else {

// // 			return false
// // 		}
// // 	})
// // }
// // // End of Creating Product (Admin only)


// // // Start of Retrieving all products (Admin Only)
// // module.exports.getAllProducts = (userData) => {

// // 	return User.findById(userData.id).then(result => {

// // 		if(result.isAdmin === true) {

// // 			return Product.find({}).then(result => {

// // 				if(result.length > 0) {

// // 					return result
// // 				} else{

// // 					return 'No active products available'
// // 				}
// // 			})
// // 		} else {

// // 			return 'Authorized admin only'
// // 		}
// // 	})
// // }
// // // End of Retrieving all products (Admin Only)


// // // Start of Retrieving all in active products (Admin Only)
// // module.exports.getAllInActiveProducts = (userData) => {

// // 	return User.findById(userData.id).then(result => {

// // 		if(result.isAdmin === true) {

// // 			return Product.find({isActive: false}).then(result => {

// // 				if(result.length > 0) {

// // 					return result
// // 				} else {

// // 					return 'No in-active products available'
// // 				}
// // 			})
// // 		} else {

// // 			return 'Authorized admin only'
// // 		}
// // 	})
	
// // }
// // // End of Retrieving all in active products (Admin Only)


// // // Start of Updating Product information (Admin only)
// // module.exports.updateProduct = (data, requestBody) => {

// // 	return Product.findById(data.productId).then(result => {

// // 		if(data.isAdmin === true) {

// // 			let updatedProduct = {
// // 				name: requestBody.name,
// // 				description: requestBody.description,
// // 				price: requestBody.price,
// // 				stocks: requestBody.stocks
// // 			}

// // 			return Product.findByIdAndUpdate(result._id, updatedProduct).then((product, error) => {

// // 				if(error){

// // 					return 'Product failed to update!'
// // 				} else {

// // 					return 'Product updated succesfully!'
// // 				}
// // 			})
// // 		} else {

// // 			return 'Authorized admin only'
// // 		}
// // 	}).catch(error => {

// // 		return 'Ooops something went wrong, Please try again.'
// // 	})
// // }
// // // End of Updating Product information (Admin only)


// // // Start of Archiving Product (Admin only)
// // module.exports.archiveProduct = (data) => {

// // 	return Product.findById(data.productId).then(result => {

// // 		if(data.isAdmin === true) {

// // 			result.isActive = false;

// // 			return result.save().then((archivedProduct,error) => {

// // 				if(error) {

// // 					return 'Product failed to archive'
// // 				} else {

// // 					return 'Product archived succesfully'
// // 				}
// // 			})
// // 		} else {

// // 			return 'Authorized admin only'
// // 		}
// // 	})
// // }
// // // End of Archiving Product (Admin only)


// // // Start of Enable Product (Admin only)
// // module.exports.enableProduct = (data) => {

// // 	return Product.findById(data.productId).then(result => {

// // 		if(data.isAdmin === true) {

// // 			result.isActive = true;
			
// // 			return result.save().then((enabledProduct,error) => {

// // 				if(error) {

// // 					return 'Product failed to enable'
// // 				} else {

// // 					return 'Product enabled succesfully'
// // 				}
// // 			})
// // 		} else {

// // 			return 'Authorized admin only'
// // 		}
// // 	})
// // }
// // // End of Enable Product (Admin only)


// // // Start of retrieving all orders (Admin only)
// // module.exports.getAllOrders = (userData) => {

// // 	return User.findById(userData.id).then(user => {

// // 		if(user.isAdmin === true) {

// // 			return Order.find().then((result, error) => {
// // 				if(error) {

// // 					return false
// // 				} else {

// // 					return result
// // 				}
// // 			})
// // 		} else {

// // 			return 'Authorized admin only'
// // 		}
// // 	})
// // }
// // // End of retrieving all orders (Admin only)

	
// // // Start of Retrieving all pending order(Admin Only)
// // module.exports.getAllPendingOrders = (userData) => {

// // 	return User.findById(userData.id).then(user => {

// // 		if (user.isAdmin === true) {

// // 			return Order.find({isDelivered: false}).then((result, error) => {

// // 				if(result.length === 0 ) {

// // 					return 'No pending orders'
// // 				}
// // 				if(error) {

// // 					return false
// // 				} else {

// // 					return result
// // 				}
// // 			})
// // 		} else {

// // 			return 'Authorized admin only'
// // 		}
// // 	})
// // }
// // // End of Retrieving all pending order(Admin Only)

// // // Start if Setting status to delivered (Admin Only)
// // module.exports.setStatusToDelivered = (data) => {

// // 	if(data.isAdmin === true) {
// // 		return Order.findByIdAndUpdate(data.orderId, {isDelivered: true} , {new: true}).then((result,error) => {
// // 			if(error) {
// // 				return false
// // 			}

// // 			if(result.isDelivered === true) {

// // 				return 'Order is already set to delivered.'
// // 			}
// // 			return result
// // 		})
// // 	} else {

// // 		return 'Authorized admin only'
// // 	}
// // }
// // // Start if Setting status to delivered (Admin Only)