const Product = require('../models/Product');
const User = require('../models/User');

// Start of Creating Product (Admin only)
module.exports.addProduct = (data, requestBody) => {
	const {id, isAdmin} = data

	return User.findById(id).then(result => {

		if(result.isAdmin) { 

			let newProduct = new Product ({

				name: requestBody.name,
				description: requestBody.description,
				price: requestBody.price,
				stocks: requestBody.stocks,
                category: requestBody.category,
				image: requestBody.image
			})

			return newProduct.save().then((product, error) => {

                if(error) {

                    return false
                } else {

                    return true
                }
            })	
		} else {

			return false
		}
	})
}
// End of Creating Product (Admin only)

//Start of Retrieving all active products
module.exports.getActiveProduct = () => {

	return Product.find({isActive: true}).then(result => {

		return result;
	})
}
//End of Retrieving all active products

//Start of Retrieving all courses (Admin Only)
module.exports.getAllProduct = () => {

	return Product.find({}).then(result => {

		return result;
	
	})

}
//End of Retrieving all courses (Admin Only)


// Start of Retrieving single product
module.exports.getSpecificProduct = (requestParams) => {

	return Product.findById(requestParams.productId).then(result => {

		return result
	})
}
// End of Retrieving single product


// Start of Updating Product information (Admin only)
module.exports.updateProduct = (data, requestBody) => {

	return Product.findById(data.productId).then(result => {

		if(data.isAdmin === true) {

			let updatedProduct = {
				name: requestBody.name,
				description: requestBody.description,
				price: requestBody.price,
				stocks: requestBody.stocks,
                category: requestBody.category
			}

			return Product.findByIdAndUpdate(result._id, updatedProduct).then((product, error) => {

				if(error){

					return false
				} else {

					return true
				}
			})
		} else {

			return false
		}
	})
}
// End of Updating Product information (Admin only)

// Start of Archiving Product (Admin only)
module.exports.archiveProduct = (data, requestBody) => {
    console.log(data)
	return Product.findById(data.productId).then(result => {

		if (data.payload === true) {

			let updateActiveField = {
				isActive: requestBody.isActive
			}

            console.log(result)
			return Product.findByIdAndUpdate(result._id, updateActiveField).then((product, err) => {

					if(err) {

						return false
					}  else {

						return true
					}
			})
		} else {

			return false
		} 
	})

}
// End of Archiving Product (Admin only)
