const User = require('../models/User');
const Order = require('../models/Order');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth')

// Start of Checking if the user's email already exist
module.exports.checkEmailExists = (requestBody) => {

	return User.find({email: requestBody.email}).then(result => {

		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
}
// End of Checking if the user's email already exist

// Start of User registration (User,Admin)
module.exports.registerUser = (requestBody) => {
	
	return User.find({email:requestBody.email}).then(result => {

		if (result.length > 0) {

			return 'Your email is already registed!'
		} else {

			let newUser = new User({
				firstName: requestBody.firstName,
				lastName: requestBody.lastName,
				email: requestBody.email,
				mobileNo: requestBody.mobileNo,
				password: bcrypt.hashSync(requestBody.password, 10),
				address: requestBody.address
			})

			return newUser.save().then((user, error) => {

				if(error) {

					return false
				} else {

					return true
				}
			})
		}
	})
}
// End of User registration


// Start of User Authentication
module.exports.loginUser = (requestBody) => {

	return User.findOne({email: requestBody.email}).then(result => {

		if(result == null) {
			
			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password);

			if(isPasswordCorrect) {

				return { access: auth.createAccessToken(result)}
			} else {

				return false
			}
		}
	})
}
// End of User Authentication


// Start of Retrieving the details of a user/admin.
module.exports.getProfile = (data) => {

	return User.findById(data.id).then(result => {

		result.password = "";
		return result;
	});

};

// End of Retrieving the details of a user/admin.

// Start of adding cart (User only)
module.exports.addToCart = (data) => {

	const {productId, userData, quantity} = data

	let counter = 0;
	let index = 0;

	if(userData.isAdmin === false) {
		return Product.findById(productId).then(product => {
			return User.findById(userData.id).then(user => {
				for(let i = 0; i < user.userCart.length; i++) {
					if (user.userCart[i].productId !== productId) {
						continue
					} else {
						counter++
						index = i
					}
				}

				if (counter > 0) {
					user.userCart[index].quantity += quantity

					return user.save().then((result, error) => {
						if (error) {
							return false
						} else 
							return true //Product already exists. Quantity Added!
					})
				} else {

					user.userCart.push({
						productId: productId, 
						productName: product.name,
						description: product.description,
						category: product.category,
						quantity: quantity,
						price: product.price
					})

					return user.save().then((result, error) => {
						if (error) {
							return false
						} else {
							return true // Successfully Added to Cart
						}
					})	
				}
			})
		})
	}
}
// End of adding cart (User only)

//Start of Removing specific product in cart (User only)
module.exports.removeToCart = (data) => {

	const { productId, userData } = data

	if(userData.isAdmin === false) {
		return User.findById(userData.id).then(user => {
			for(let i = 0; i < user.userCart.length; i++) {
				if (user.userCart[i].productId === productId) {
				user.userCart.splice(i, 1)

				return user.save().then((result, error) => {
					if (error) {
						return false  
					} else {
						return true
					}
				})	
			}
			}
		})
	} else {
		return {error: `Users Only`}
	}
}
//End of Removing specific product in cart (User only)


// // Start of adding cart (User only)
// module.exports.addCart = async (userData, requestBody) => {

//     if(userData.isAdmin === false) {

// 		let isUserUpdated;
// 		let isProductUpdated;

//         for (let i = 0; i < requestBody.products.length; i++) {

//             let addedProducts = await Product.findById(requestBody.products[i].productId)

//             let data = {
//                 productId: addedProducts.id,
//                 productName: addedProducts.name,
//                 quantity: requestBody.products[i].quantity,
//                 price: addedProducts.price,
//                 subTotal: requestBody.products[i].quantity*addedProducts.price
//             }

//             if (data.quantity > 0) {

//             	if (addedProducts.stocks !== 0 && addedProducts.stocks >= data.quantity) {

//             		isUserUpdated = await User.findById(userData.id).then(user => {

//             			user.userCart.push(data);

//             			return user.save().then((result, error) => {
//             				if(error) {
//             					return false
//             				} else {
//             					return true
//             				}
//             			})
//             		})


//             		isProductUpdated = await Product.findById(data.productId).then(product => {

//             			product.stocks -= data.quantity;

//             			return product.save().then((result, error) => {
//     			            if (error) {
//     			                return false
//     			            } else {
//     			            	return true
//     			            }
    			            
//     			        })
//             		})

// 	            } else {

// 	            	return `${data.productName} is currently out of stock. We have ${addedProducts.stocks} stock(s) for this item.`
// 	            }
//             } else {

//             	return 'Quantity is not valid. The quantity must be a valid number greater than 0'
//             }
//         }
//         if (isUserUpdated && isProductUpdated) {

//         	return 'SUCCESS: Added to cart'
//         } else {

//         	return 'Failed to add cart, Please try again!'
//         }
//     } else {

//     	return 'Only user can create order.'
//     }
// }
// // End of adding cart (User only)


// Start of Retriving user cart (User only)
module.exports.myCart = (userData) => {

	if(userData.isAdmin === false){

		return User.findById(userData.id).then(user => {

			if(user.userCart.length === 0) {

				return []
			} else {

				return user.userCart
			}
		})
	}
}
// End of Retriving user cart (User only)

//Start of Getting total price of cart (User only)
module.exports.getTotalPriceCart = (userData) => {
	const {id, isAdmin} = userData

	let arrayOfTotal = [];
	let totalPrice= 0;

	if(isAdmin === false) {
		return User.findById(id).then(user => {
			if(user.userCart.length > 0) {
				let cartedProduct = user.userCart.map(product => {
					return product
				})
				
				for (let i = 0; i < cartedProduct.length; i++) {
					arrayOfTotal.push(cartedProduct[i].quantity*cartedProduct[i].price)
				}

				for (i = 0; i < arrayOfTotal.length; i++) {
					totalPrice += arrayOfTotal[i]
				}

				return {totalPrice: totalPrice}
			} else {
				return false
			}
		})
	}
}
//End of Getting total price of cart (User only)


//Start of Editting user cart (User only)
module.exports.editCart = (data) => {
	const { productId, userData, quantity} = data

	if(userData.isAdmin === false) {

		return User.findById(userData.id).then(user => {

			for(let i = 0; i < user.userCart.length; i++) {

				if(user.userCart[i].productId === productId) {

					user.userCart[i].quantity = quantity

					return user.save().then((result,error) => {

						if(error){

							return false
						} else {

							return true
						}
					})
				}
			}
		})
	}
}
//End of Editting user cart (User only)

//Start of Checkout (User Only)

module.exports.checkOut = async (userData) => {

	const {id, email, isAdmin} = userData

	let productId = [];
	let description = [];
	let quantity = [];
	let unitPrice = [];
	let totalPrice = 0;

	let isUserUpdated = false;
	let isProductUpdated = false;
	let isOrderUpdated = false;

	if(isAdmin === false){
		isOrderUpdated = await User.findById(id).then(user => {

			if(user.userCart.length > 0) {

				let totalAmount = 0
				for (let i = 0; i < user.userCart.length; i++) {
					totalAmount += user.userCart[i].price * user.userCart[i].quantity
				}

				let newOrder = new Order({
					userId: id,
					email: email,
					products: user.userCart,
					totalAmount: totalAmount
				})

				return newOrder.save().then((result, error) => {
					if(error){

						return false
					} else {

						return true
					}
				})
			} else {

				return false
			}
		})

		isUserUpdated = await User.findById(id).then(user => {
			if(user.userCart.length > 0){
				
				for( let i = 0; i < user.userCart.length; i++){
					productId.push(user.userCart[i].productId)
					description.push(user.userCart[i].description)
					quantity.push(user.userCart[i].quantity)
					unitPrice.push(user.userCart[i].price)

					totalPrice += (user.userCart[i].quantity * user.userCart[i].price)
				}

				let order = {
					userId: id,
					description: description,
					products: user.userCart,
					totalPrice: totalPrice,
				}

				user.orders.push(order)

				user.userCart = []

				return user.save().then((result, error) => {
					if (error) {
						return false
					} else {
						return true
					}
				})
			} else {
				return false
			}
		})

		for (let i = 0; i < productId.length; i++) {
			isProductUpdated = await Product.findById(productId[i]).then(product => {

				let orderedQuantity = quantity[i]
		
				product.stocks -= orderedQuantity

				if (product.stocks === 0) {
					product.isActive = false
				}

				return product.save().then((result, error) => {
					if (error) {
						return false
					} else {
						return true
					}
				})
			})
		}
	}

	if (isUserUpdated  && isProductUpdated && isOrderUpdated) {
		return true
	} else {
		return false
	}

	console.log(`isUserUpdated : ${isUserUpdated}`)
	console.log(`isProductUpdated : ${isProductUpdated}`)
	console.log(`isOrderUpdated : ${isOrderUpdated}`)
}
//End of Checkout (User Only)

// Start of Retriving user order (User only)
module.exports.myOrder = (userData) => {

	const {id, isAdmin} = userData

	if(isAdmin === false) {

		return User.findById(id).then(result => {
			
			if(result.orders.length <= 0 ) {
				return false
			} else {

				return result.orders
			}
		})
	}
}
// End of Retriving user order (User only)

// Start of retrieving all orders (Admin only)
module.exports.getAllOrders = (userData) => {

	return User.findById(userData.id).then(user => {

		if(user.isAdmin) {

			return Order.find().then((result, error) => {
				if(error) {

					return false
				} else {

					return result
				}
			})
		} else {

			return false
		}
	})
}
// End of retrieving all orders (Admin only)

// Start of Retrieve all users (Admin only)
module.exports.getAllUsers = (userData) => {

	return User.findById(userData.id).then(result => {

		if(result.isAdmin === true) {
			return User.find({isAdmin:false}).then(result => {
				if (result.length > 0) {
					return result
				} else {
					return {alert: `Empty users`}
				}   
			})
		} else {
			return false
		}
	})
}
// End of Retrieve all users (Admin only)


// // Start of checkout users order (User only)
// module.exports.checkOut = async (userData) => {

// 	if (userData.isAdmin === false) {

		// let isOrderUpdated = await User.findById(userData.id).then(user => {

		// 	if(user.userCart.length > 0) {

				// let totalAmount = 0
				// for (let i = 0; i < user.userCart.length; i++) {
				// 	totalAmount += user.userCart[i].subTotal
				// }

		// 		let newOrder = new Order({
		// 			userId: userData.id,
		// 			email: userData.email,
		// 			products: user.userCart,
		// 			totalAmount: totalAmount
		// 		})

		// 		return newOrder.save().then((result, error) => {
		// 			if(error){

		// 				return false
		// 			} else {

		// 				return true
		// 			}
		// 		})
		// 	} else {

		// 		return 'Empty'
		// 	}
		// })

// 		let isUserUpdated = await User.findById(userData.id).then(user => {


// 			user.userCart = []

// 			return user.save().then((result, error) => {
// 	            if (error) {
// 	                return false;
// 	            } else {

// 	            	return true
// 	            }
// 	        })
// 		})

// 		if(isOrderUpdated === 'Empty') {

// 			return 'Your cart is empty'
// 		} else {

// 			return 'Thank you for ordering, We received your order and will begin processing it soon.'
// 		}
// 	} else {

// 		return 'Users only'
// 	}
// }
// // End of checkout users order (User only)



