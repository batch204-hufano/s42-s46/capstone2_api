const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First Name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	address: {
		type: String,
		required: [true, "Address is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	registeredOn: {
		type: Date,
		default: new Date()
	},
	userCart : [
        {
            productId: {
                type: String,
                required: [true, 'Product ID is required']
            },
            productName: {
                type: String,
                required: [true, 'Product name is required']
            },
			description: {
                type: String,
                required: [true, 'Product description is required']
            },
			category: {
                type: String,
                required: [true, 'Product category is required']
            },
            quantity: {
                type: Number,
                required: [true, 'Product quantity is required']
            },
            price: {
                type: Number,
                required: [true, 'Product price is required']
            },
			subTotal: {
                type: Number
            },
            addedOn: {
                type: Date,
                default: new Date()
            }
        }
    ],
	orders: [
		{
			userId: {
				type: String,
				required: [true, "User id is required."]
			},
			status: {
				type: String,
				default: "Purchased"
			},
			products: [
				{
					productId: {
						type: String,
						required: [true, "Product ID is required."]
					},
					description: {
						type: String,
						required: [true, 'Product description is required']
					},
					quantity: {
						type: Number,
						required: [true, "Quantity is required."]
					},
					price: {
						type: Number,
						required: [true, "Unit price is required."]
					}
				}
			],
			totalPrice: {
				type: Number,
				required: [true, "Please compute total Price"]
			},
			orderedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})


module.exports = mongoose.model("User", userSchema);