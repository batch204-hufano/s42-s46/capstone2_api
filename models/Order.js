const mongoose = require('mongoose');


const orderSchema = new mongoose.Schema({

	userId : { 
		type: String,
		require: [true, 'User ID is required']
	},
	email : { 
		type: String,
		require: [true, 'User email is required']
	},
	products : [
        {
            productId: {
                type: String,
                required: [true, 'Product ID is required']
            },
            productName: {
                type: String,
                required: [true, 'Product name is required']
            },
            quantity: {
                type: Number,
                required: [true, 'Product quantity is required']
            },
            price: {
                type: Number,
                required: [true, 'Product price is required']
            },
        }
    ],
	totalAmount: {
		type: Number,
		default: 0
	},
	isDelivered: {
		type: String,
		default: "Pending"
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})


module.exports = mongoose.model("Order", orderSchema);