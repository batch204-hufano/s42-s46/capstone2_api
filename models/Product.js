const mongoose = require('mongoose');


const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, 'Product name is required']
	},
	description: {
		type: String,
		required: [true, 'Product description is required']
	},
	price: {
		type: Number,
		required: [true, 'Product price is required']
	},
	stocks: {
		type: Number,
		required: [true, 'Product stocks is required']
	},
	category: {
		type: String,
		required: [true, 'Product category is required']
	},
	image: {
		type: String,
		required: [true, 'Product image is required']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
})


module.exports = mongoose.model("Product", productSchema);