const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

//Checking if the user's email already exist
router.post('/checkEmail', (request, response) => {
	userController.checkEmailExists(request.body).then(resultFromController => response.send(resultFromController));
});

// User registration (User/Admin)
router.post('/register', (request, response) => {
	userController.registerUser(request.body).then(resultFromController => response.send(resultFromController))
})


// User Authentication
router.post("/login", (request,response) => {
	userController.loginUser(request.body).then(resultFromController => response.send(resultFromController))
});


// Retrieve details of a user/admin.
router.get("/profile", auth.verify, (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	userController.getProfile({id : userData.id}).then(resultFromController => response.send(resultFromController));
});


// Add cart (User only)
router.post('/:productId/addToCart', auth.verify, (request, response) => {
	const data = {
		productId: request.params.productId,
		userData :auth.decode(request.headers.authorization),
		quantity: request.body.quantity
	}
	userController.addToCart(data).then(resultFromController => response.send(resultFromController))
})

// Retrive user cart (User only)
router.get('/cart', auth.verify , (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	userController.myCart(userData).then(resultFromController => response.send(resultFromController))
})

//Remove specific product in cart (User only)
router.delete('/:productId/removeToCart', auth.verify, (request, response) => {
	const data = {
		productId: request.params.productId,
		userData : auth.decode(request.headers.authorization)
	}
	userController.removeToCart(data).then(resultFromController => response.send(resultFromController))
})

//Edit user cart (User only)
router.put('/:productId/cart', auth.verify, (request, response) => {
	const data = {
		productId: request.params.productId,
		userData: auth.decode(request.headers.authorization),
		quantity: request.body.quantity
	}
	userController.editCart(data).then(resultFromController => response.send(resultFromController))
})


//Get total price of cart (User only)
router.get('/cart/total', auth.verify, (request, response) => {
	const userData  = auth.decode(request.headers.authorization)
	userController.getTotalPriceCart(userData).then(resultFromController => response.send(resultFromController))
})

// Check out (User only)
router.post('/checkout' , auth.verify, (request, response) => {
	const userData  = auth.decode(request.headers.authorization)
	userController.checkOut(userData).then(resultFromController => response.send(resultFromController))
})

// Retrive user order (User only)
router.get('/myOrder' , auth.verify, (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	userController.myOrder(userData).then(resultFromController => response.send(resultFromController))
})


// Retrieve all orders (Admin only)
router.get('/orders' , auth.verify, (request,response) => {
	const userData = auth.decode(request.headers.authorization)
	userController.getAllOrders(userData).then(resultFromController => response.send(resultFromController))
})

// Retrieve all users (Admin only)
router.get('/' , auth.verify, (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	userController.getAllUsers(userData).then(resultFromController => response.send(resultFromController))
})


// // User order (User only)
// router.post('/checkOut', auth.verify, (request,response) => {
// 	const userData = auth.decode(request.headers.authorization)
// 	userController.checkOut(userData).then(resultFromController => response.send(resultFromController))
// })





module.exports = router;