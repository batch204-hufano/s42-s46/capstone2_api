const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');


// Retrieve all active products
router.get('/', (request, response) => {
	productController.getActiveProduct().then(resultFromController => response.send(resultFromController))
})

// Create Product (Admin only)
router.post('/add', auth.verify, (request, response) => {
	const data = auth.decode(request.headers.authorization)
	productController.addProduct(data, request.body).then(resultFromController => response.send(resultFromController))
})

// Retrieve all products (Admin only)
router.get("/all", (request, response) => {
	productController.getAllProduct().then(resultFromController => response.send(resultFromController))
});

// Retrieve single product
router.get('/id/:productId', (request, response) => {
	productController.getSpecificProduct(request.params).then(resultFromController => response.send(resultFromController))
})

// Create Product (Admin only)
router.post('/products/add', auth.verify, (request, response) => {
	const data = auth.decode(request.headers.authorization)
	productController.addProduct(data, request.body).then(resultFromController => response.send(resultFromController))
})

// Update Product information (Admin only)
router.put('/:productId', auth.verify, (request,response) => {
	const data = {
		productId: request.params.productId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	productController.updateProduct(data, request.body).then(resultFromController => response.send(resultFromController))
})

router.put('/:productId/archive', auth.verify, (request, response) => {
	const data = {
		productId : request.params.productId,
		payload : auth.decode(request.headers.authorization).isAdmin
	}
	productController.archiveProduct(data, request.body).then(resultFromController => response.send(resultFromController))
});


module.exports = router;