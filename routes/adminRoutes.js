// const express = require('express');
// const router = express.Router();
// const adminController = require('../controllers/adminController');
// const auth = require('../auth');


// // Retrieve all users (Admin only)
// router.get('/users/all' , auth.verify, (request, response) => {
// 	const userData = auth.decode(request.headers.authorization)
// 	adminController.getAllUsers(userData).then(resultFromController => response.send(resultFromController))
// })


// // Set user as admin (Admin only)
// router.put('/users/:userId/setAdmin' , auth.verify , (request, response) => {
// 	const data = {
// 		userId: request.params.userId,
// 		isAdmin: auth.decode(request.headers.authorization).isAdmin
// 	}
// 	adminController.setAdmin(data).then(resultFromController => response.send(resultFromController))
// })


// //Set Admin to user (Admin only)
// router.put('/users/:userId/removeAdmin' , auth.verify , (request, response) => {
// 	const data = {
// 		userId: request.params.userId,
// 		isAdmin: auth.decode(request.headers.authorization).isAdmin
// 	}
// 	adminController.removeAdmin(data).then(resultFromController => response.send(resultFromController))
// })



// // Create Product (Admin only)
// router.post('/products/add', auth.verify, (request, response) => {
// 	const data = auth.decode(request.headers.authorization)
// 	adminController.addProduct(data, request.body).then(resultFromController => response.send(resultFromController))
// })


// // Retrieve all products (Admin Only)
// router.get('/products/all', auth.verify, (request, response) => {
// 	const userData = auth.decode(request.headers.authorization)
// 	adminController.getAllProducts(userData).then(resultFromController => response.send(resultFromController))
// })


// // Retrieve all in active products (Admin Only)
// router.get('/products/all/inActive', auth.verify, (request, response) => {
// 	const userData = auth.decode(request.headers.authorization)
// 	adminController.getAllInActiveProducts(userData).then(resultFromController => response.send(resultFromController))
// })


// // // Update Product information (Admin only)
// // router.put('/products/:productId/update', auth.verify, (request,response) => {
// // 	const data = {
// // 		productId: request.params.productId,
// // 		isAdmin: auth.decode(request.headers.authorization).isAdmin
// // 	}
// // 	adminController.updateProduct(data, request.body).then(resultFromController => response.send(resultFromController))
// // })


// //Archive Product (Admin only)
// router.put('/products/:productId/archive', auth.verify , (request, response) => {
// 	const data = {
// 		productId: request.params.productId,
// 		isAdmin: auth.decode(request.headers.authorization).isAdmin
// 	}
// 	adminController.archiveProduct(data).then(resultFromController => response.send(resultFromController));
// });


// //Enable Product (Admin only)
// router.put('/products/:productId/enable', auth.verify , (request, response) => {
// 	const data = {
// 		productId: request.params.productId,
// 		isAdmin: auth.decode(request.headers.authorization).isAdmin
// 	}
// 	adminController.enableProduct(data).then(resultFromController => response.send(resultFromController));
// });

// // Retrieve all orders (Admin only)
// router.get('/orders/all' , auth.verify, (request,response) => {
// 	const userData = auth.decode(request.headers.authorization)
// 	adminController.getAllOrders(userData).then(resultFromController => response.send(resultFromController))
// })


// // Retrieve all pending order (Admin Only)
// router.get('/orders/pending' , auth.verify, (request,response) => {
// 	const userData = auth.decode(request.headers.authorization)
// 	adminController.getAllPendingOrders(userData).then(resultFromController => response.send(resultFromController))
// })


// // Set status to delivered (Admin Only)
// router.put('/orders/:orderId/delivered' , auth.verify, (request,response) => {
// 	const data = {
// 		orderId: request.params.orderId,
// 		isAdmin: auth.decode(request.headers.authorization).isAdmin
// 	}
// 	adminController.setStatusToDelivered(data).then(resultFromController => response.send(resultFromController))
// })


// module.exports = router;